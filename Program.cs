﻿using OpenQA.Selenium;
using System;
using OpenQA.Selenium.Chrome;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {

            IWebDriver driver = new ChromeDriver();
            driver.Url = "https://bridal.miiduu.com/";
            driver.Manage().Window.Maximize();
            IWebElement tabBridalGowns = driver.FindElement(By.XPath("//a[@href='/bridal-gowns']"));
            tabBridalGowns.Click();
            Thread.Sleep(100);
            IWebElement lblQuasiArchitecto = driver.FindElement(By.XPath("//a[text()='Quasi Architecto']"));
            lblQuasiArchitecto.Click();
            Thread.Sleep(2000);
            IWebElement tbnAddtocart = driver.FindElement(By.XPath("//span[contains(text(),'Add to Cart')]"));
            tbnAddtocart.Click();
            Thread.Sleep(100);
            IWebElement btnProceedtoCheckout = driver.FindElement(By.XPath("//span[text()='Proceed to Checkout']"));
            btnProceedtoCheckout.Click();
            Thread.Sleep(100);
            IWebElement rdoguest = driver.FindElement(By.XPath("//input[@id='guest']"));
            rdoguest.Click();
            Thread.Sleep(100);
            IWebElement btncontinue = driver.FindElement(By.XPath("//span[text()='Continue']"));
            btncontinue.Click();
            Thread.Sleep(100);
            IWebElement txtFirstname = driver.FindElement(By.XPath("//input[@name='firstname']"));
            txtFirstname.SendKeys("hieu");
            Thread.Sleep(100);
            IWebElement txtLastname = driver.FindElement(By.XPath("//input[@name='lastname']"));
            txtLastname.SendKeys("huynh");
            Thread.Sleep(100);
            IWebElement txtEmail = driver.FindElement(By.XPath("//input[@name='email']"));
            txtEmail.SendKeys("hieu@gmail.com");
            Thread.Sleep(100);
            IWebElement txtTelephone = driver.FindElement(By.XPath("//input[@name='telephone']"));
            txtTelephone.SendKeys("123456789");
            Thread.Sleep(100);
            IWebElement txtFax = driver.FindElement(By.XPath("//input[@name='fax']"));
            txtFax.SendKeys("112");
            Thread.Sleep(100);
            IWebElement txtCompany = driver.FindElement(By.XPath("//input[@name='company']"));
            txtCompany.SendKeys("FPT");
            Thread.Sleep(100);
            IWebElement txtAddress_1 = driver.FindElement(By.XPath("//input[@name='address_1']"));
            txtAddress_1.SendKeys("phu my");
            Thread.Sleep(100);
            IWebElement txtAddress_2 = driver.FindElement(By.XPath("//input[@name='address_2']"));
            txtAddress_2.SendKeys("binh dinh");
            Thread.Sleep(100);
            IWebElement txtCity = driver.FindElement(By.XPath("//input[@name='city']"));
            txtCity.SendKeys("binh dinh");
            Thread.Sleep(100);
            IWebElement txtPostcode = driver.FindElement(By.XPath("//input[@name='postcode']"));
            txtPostcode.SendKeys("1111");
            // select the drop down list
            var lstCountry_id = driver.FindElement(By.Name("country_id"));
            //create select element object 
            var selectElement = new SelectElement(lstCountry_id);
            //select by value
            selectElement.SelectByValue("203");
            // select by text
            selectElement.SelectByText("Viet Nam");
            //
            Thread.Sleep(1000);
            // select the drop down list
            var lstZone_id = driver.FindElement(By.Name("zone_id"));
            //create select element object 
            var selectElement1 = new SelectElement(lstZone_id);
            //select by value
            selectElement1.SelectByValue("3758");
            // select by text
            selectElement1.SelectByText("Binh Dinh");
            Thread.Sleep(100);  
            //IWebElement chkrecaptcha = driver.FindElement(By.CssSelector("div[class='recaptcha-checkbox-border']"));
           // Actions actions = new Actions(driver);
           // actions.MoveToElement(chkrecaptcha).Click();
            // chkrecaptcha.Click();
            Thread.Sleep(100);
            IWebElement rdoshipping = driver.FindElement(By.XPath("//input[@id='shipping_indicator']"));
            rdoshipping.Click();
            IWebElement txtFirstnameship = driver.FindElement(By.XPath("//input[@name='shipping_firstname']"));
            txtFirstnameship.SendKeys("hieu");
            Thread.Sleep(100);
            IWebElement txtLastnameship = driver.FindElement(By.XPath("//input[@name='shipping_lastname']"));
            txtLastnameship.SendKeys("huynh");
            Thread.Sleep(100);
            IWebElement txtCompanyship = driver.FindElement(By.XPath("//input[@name='shipping_company']"));
            txtCompanyship.SendKeys("FPT");
            Thread.Sleep(100);
            IWebElement txtAddress_1ship = driver.FindElement(By.XPath("//input[@name='shipping_address_1']"));
            txtAddress_1ship.SendKeys("phu my");
            Thread.Sleep(100);
            IWebElement txtAddress_2ship = driver.FindElement(By.XPath("//input[@name='shipping_address_2']"));
            txtAddress_2ship.SendKeys("binh dinh");
            Thread.Sleep(100);
            IWebElement txtCityship = driver.FindElement(By.XPath("//input[@name='shipping_city']"));
            txtCityship.SendKeys("binh dinh");
            Thread.Sleep(100);
            IWebElement txtPostcodeship = driver.FindElement(By.XPath("//input[@name='shipping_postcode']"));
            txtPostcodeship.SendKeys("1111");
            // select the drop down list
            var lstCountry_idship = driver.FindElement(By.Name("shipping_country_id"));
            //create select element object 
            var selectElementship = new SelectElement(lstCountry_idship);
            //select by value
            selectElementship.SelectByValue("203");
            // select by text
            selectElementship.SelectByText("Viet Nam");
            //
            Thread.Sleep(1000);
            // select the drop down list
            var lstZone_idship = driver.FindElement(By.Name("shipping_zone_id"));
            //create select element object 
            var selectElement1ship = new SelectElement(lstZone_idship);
            //select by value
            selectElement1ship.SelectByValue("3758");
            // select by text
            selectElement1ship.SelectByText("Binh Dinh");
            Thread.Sleep(100);
            Thread.Sleep(100);
            IWebElement btnContinue1 = driver.FindElement(By.XPath("//span[text()='Continue']"));
            btnContinue1.Click();
            Thread.Sleep(100);
            IWebElement rdoagreeship = driver.FindElement(By.XPath("//input[@name='agree']"));
            rdoagreeship.Click();
            Thread.Sleep(100);
            IWebElement bntContinue2 = driver.FindElement(By.XPath("//span[text()='Continue']"));
            bntContinue2.Click();
            Thread.Sleep(100);
            IWebElement btnConfirm = driver.FindElement(By.XPath("//span[text()='Confirm Order']"));
            btnConfirm.Click();
            Thread.Sleep(2000);
            IWebElement bntContinue3 = driver.FindElement(By.XPath("//span[text()='Continue']"));
            bntContinue3.Click();

            //driver.Quit();
            Console.WriteLine("Hello World!");
        }
    }
}
